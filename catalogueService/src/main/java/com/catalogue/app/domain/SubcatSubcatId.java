package com.catalogue.app.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the subcat_subcat_id database table.
 * 
 */
@Entity
@Table(name="subcat_subcat_id")
@NamedQuery(name="SubcatSubcatId.findAll", query="SELECT s FROM SubcatSubcatId s")
public class SubcatSubcatId extends AbstractAuditingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	//bi-directional many-to-one association to Subcategory
	@ManyToOne(fetch=FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name="subcat_id")
	private Subcategory subcategory1;

	//bi-directional many-to-one association to Subcategory
	@ManyToOne(fetch=FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name="subcat_sub_id")
	private Subcategory subcategory2;

	public SubcatSubcatId() {
	}


    public SubcatSubcatId(Subcategory subcategory1, Subcategory subcategory2) {
        this.subcategory1 = subcategory1;
        this.subcategory2 = subcategory2;
    }

    public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Subcategory getSubcategory1() {
		return this.subcategory1;
	}

	public void setSubcategory1(Subcategory subcategory1) {
		this.subcategory1 = subcategory1;
	}

	public Subcategory getSubcategory2() {
		return this.subcategory2;
	}

	public void setSubcategory2(Subcategory subcategory2) {
		this.subcategory2 = subcategory2;
	}

}
