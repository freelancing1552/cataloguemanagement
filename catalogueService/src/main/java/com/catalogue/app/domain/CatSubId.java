package com.catalogue.app.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cat_sub_id database table.
 * 
 */
@Entity
@Table(name="cat_sub_id")
@NamedQuery(name="CatSubId.findAll", query="SELECT c FROM CatSubId c")
public class CatSubId extends AbstractAuditingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	//bi-directional many-to-one association to Category
	@ManyToOne(fetch=FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name="cat_id")
	private Category category;

	//bi-directional many-to-one association to Subcategory
	@ManyToOne(fetch=FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name="subcat_id")
	private Subcategory subcategory;

	public CatSubId() {
	}

    public CatSubId(Category category, Subcategory subcategory) {
        this.category = category;
        this.subcategory = subcategory;
    }

    public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Subcategory getSubcategory() {
		return this.subcategory;
	}

	public void setSubcategory(Subcategory subcategory) {
		this.subcategory = subcategory;
	}

}
