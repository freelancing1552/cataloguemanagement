package com.catalogue.app.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the subcategory database table.
 * 
 */
@Entity
@NamedQuery(name="Subcategory.findAll", query="SELECT s FROM Subcategory s")
public class Subcategory extends AbstractAuditingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

    private String name;

    @Transient
    private Integer categoryId;

    @Transient
    private Integer subCategoryId;


	//bi-directional many-to-many association to Category
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
		name="cat_sub_id"
		, joinColumns={
			@JoinColumn(name="subcat_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="cat_id")
			})
	private List<Category> categories;

	//bi-directional many-to-many association to Product
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
		name="subcat_prod_id"
		, joinColumns={
			@JoinColumn(name="subcat_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="prod_id")
			}
		)
	private List<Product> products;

	//bi-directional many-to-one association to CatSubId
	@OneToMany(mappedBy="subcategory",cascade = CascadeType.ALL)
	private List<CatSubId> catSubIds;

	//bi-directional many-to-one association to SubcatProdId
	@OneToMany(mappedBy="subcategory",cascade = CascadeType.ALL)
	private List<SubcatProdId> subcatProdIds;

	//bi-directional many-to-one association to SubcatSubcatId
	@OneToMany(mappedBy="subcategory1",cascade = CascadeType.ALL)
	private List<SubcatSubcatId> subcatSubcatIds1;

	//bi-directional many-to-one association to SubcatSubcatId
	@OneToMany(mappedBy="subcategory2",cascade = CascadeType.ALL)
	private List<SubcatSubcatId> subcatSubcatIds2;

	public Subcategory() {
	}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Category> getCategories() {
		return this.categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public List<Product> getProducts() {
		return this.products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public List<CatSubId> getCatSubIds() {
		return this.catSubIds;
	}

	public void setCatSubIds(List<CatSubId> catSubIds) {
		this.catSubIds = catSubIds;
	}

	public CatSubId addCatSubId(CatSubId catSubId) {
		getCatSubIds().add(catSubId);
		catSubId.setSubcategory(this);

		return catSubId;
	}

	public CatSubId removeCatSubId(CatSubId catSubId) {
		getCatSubIds().remove(catSubId);
		catSubId.setSubcategory(null);

		return catSubId;
	}

	public List<SubcatProdId> getSubcatProdIds() {
		return this.subcatProdIds;
	}

	public void setSubcatProdIds(List<SubcatProdId> subcatProdIds) {
		this.subcatProdIds = subcatProdIds;
	}

	public SubcatProdId addSubcatProdId(SubcatProdId subcatProdId) {
		getSubcatProdIds().add(subcatProdId);
		subcatProdId.setSubcategory(this);

		return subcatProdId;
	}

	public SubcatProdId removeSubcatProdId(SubcatProdId subcatProdId) {
		getSubcatProdIds().remove(subcatProdId);
		subcatProdId.setSubcategory(null);

		return subcatProdId;
	}

	public List<SubcatSubcatId> getSubcatSubcatIds1() {
		return this.subcatSubcatIds1;
	}

	public void setSubcatSubcatIds1(List<SubcatSubcatId> subcatSubcatIds1) {
		this.subcatSubcatIds1 = subcatSubcatIds1;
	}

	public SubcatSubcatId addSubcatSubcatIds1(SubcatSubcatId subcatSubcatIds1) {
		getSubcatSubcatIds1().add(subcatSubcatIds1);
		subcatSubcatIds1.setSubcategory1(this);

		return subcatSubcatIds1;
	}

	public SubcatSubcatId removeSubcatSubcatIds1(SubcatSubcatId subcatSubcatIds1) {
		getSubcatSubcatIds1().remove(subcatSubcatIds1);
		subcatSubcatIds1.setSubcategory1(null);

		return subcatSubcatIds1;
	}

	public List<SubcatSubcatId> getSubcatSubcatIds2() {
		return this.subcatSubcatIds2;
	}

	public void setSubcatSubcatIds2(List<SubcatSubcatId> subcatSubcatIds2) {
		this.subcatSubcatIds2 = subcatSubcatIds2;
	}

	public SubcatSubcatId addSubcatSubcatIds2(SubcatSubcatId subcatSubcatIds2) {
		getSubcatSubcatIds2().add(subcatSubcatIds2);
		subcatSubcatIds2.setSubcategory2(this);

		return subcatSubcatIds2;
	}

	public SubcatSubcatId removeSubcatSubcatIds2(SubcatSubcatId subcatSubcatIds2) {
		getSubcatSubcatIds2().remove(subcatSubcatIds2);
		subcatSubcatIds2.setSubcategory2(null);

		return subcatSubcatIds2;
	}

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Integer subCategoryId) {
        this.subCategoryId = subCategoryId;
    }
}
