package com.catalogue.app.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the product database table.
 * 
 */
@Entity
@NamedQuery(name="Product.findAll", query="SELECT p FROM Product p")
public class Product extends AbstractAuditingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String name;

	@Transient
    private Integer subCategoryId;

	@Transient
    private Integer categoryId;

	//bi-directional many-to-many association to Subcategory
	@ManyToMany(mappedBy="products",cascade = CascadeType.ALL)
	private List<Subcategory> subcategories;

	//bi-directional many-to-one association to SubcatProdId
	@OneToMany(mappedBy="product",cascade = CascadeType.ALL)
	private List<SubcatProdId> subcatProdIds;

	public Product() {
	}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Subcategory> getSubcategories() {
		return this.subcategories;
	}

	public void setSubcategories(List<Subcategory> subcategories) {
		this.subcategories = subcategories;
	}

	public List<SubcatProdId> getSubcatProdIds() {
		return this.subcatProdIds;
	}

	public void setSubcatProdIds(List<SubcatProdId> subcatProdIds) {
		this.subcatProdIds = subcatProdIds;
	}

	public SubcatProdId addSubcatProdId(SubcatProdId subcatProdId) {
		getSubcatProdIds().add(subcatProdId);
		subcatProdId.setProduct(this);

		return subcatProdId;
	}

	public SubcatProdId removeSubcatProdId(SubcatProdId subcatProdId) {
		getSubcatProdIds().remove(subcatProdId);
		subcatProdId.setProduct(null);

		return subcatProdId;
	}

    public Integer getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Integer subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
