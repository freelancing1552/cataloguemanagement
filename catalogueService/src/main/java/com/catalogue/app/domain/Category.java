package com.catalogue.app.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the categories database table.
 * 
 */
@Entity
@Table(name="categories")
@NamedQuery(name="Category.findAll", query="SELECT c FROM Category c")
public class Category extends AbstractAuditingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String name;

	//bi-directional many-to-many association to Subcategory
	@ManyToMany(mappedBy="categories",cascade = CascadeType.ALL)
	private List<Subcategory> subcategories;

	//bi-directional many-to-one association to CatSubId
	@OneToMany(mappedBy="category",cascade = CascadeType.ALL)
	private List<CatSubId> catSubIds;

	public Category() {
	}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Subcategory> getSubcategories() {
		return this.subcategories;
	}

	public void setSubcategories(List<Subcategory> subcategories) {
		this.subcategories = subcategories;
	}

	public List<CatSubId> getCatSubIds() {
		return this.catSubIds;
	}

	public void setCatSubIds(List<CatSubId> catSubIds) {
		this.catSubIds = catSubIds;
	}

	public CatSubId addCatSubId(CatSubId catSubId) {
		getCatSubIds().add(catSubId);
		catSubId.setCategory(this);

		return catSubId;
	}

	public CatSubId removeCatSubId(CatSubId catSubId) {
		getCatSubIds().remove(catSubId);
		catSubId.setCategory(null);

		return catSubId;
	}

}
