package com.catalogue.app.web.rest;

import com.catalogue.app.domain.*;
import com.catalogue.app.dto.CategoryDto;
import com.catalogue.app.dto.ProductDto;
import com.catalogue.app.dto.SubcategoryDto;
import com.catalogue.app.repository.CategoryRepository;
import com.catalogue.app.repository.ProductRepository;
import com.catalogue.app.repository.SubCategoryRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/catalogue/categoryApi")
public class CatalogueResourceController {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    SubCategoryRepository subCategoryRepository;

    @Autowired
    ProductRepository productRepository;

    @PostMapping
    public ResponseEntity<Category> createCategory(@RequestBody CategoryDto categoryDto) {
        ObjectMapper mapper = new ObjectMapper();
        Category category = mapper.convertValue(categoryDto, Category.class);
        return ResponseEntity.ok(categoryRepository.save(category));
    }

    @GetMapping
    public ResponseEntity<List<Category>> getAllCategories() {
        return ResponseEntity.ok(Optional.of(categoryRepository.findAll()).orElse(null));
    }

    @PostMapping("/createSubCategory")
    public ResponseEntity<String> createSubCategoryMapping(@RequestBody SubcategoryDto subcategoryDto) {
        ObjectMapper mapper = new ObjectMapper();
        Category category = categoryRepository.findById(subcategoryDto.getCategoryId()).orElseThrow(RuntimeException::new);
        Subcategory subcategory = mapper.convertValue(subcategoryDto, Subcategory.class);
        category.setSubcategories(Collections.singletonList(subcategory));
        CatSubId catSubId = new CatSubId(category, subcategory);
        category.addCatSubId(catSubId);
        return saveAndCreateResponse(category);
    }

    @PostMapping("/createSubCategoryMapping")
    public ResponseEntity<String> createSubCatSubCategoryMapping(@RequestBody SubcategoryDto subcategoryDto) {
        ObjectMapper mapper = new ObjectMapper();
        Subcategory parentSubcategory = subCategoryRepository.findById(subcategoryDto.getSubCategoryId()).orElseThrow(RuntimeException::new);
        Subcategory childSubcategory = mapper.convertValue(subcategoryDto, Subcategory.class);
        SubcatSubcatId subcatSubcatId = new SubcatSubcatId(parentSubcategory, childSubcategory);
        parentSubcategory.addSubcatSubcatIds1(subcatSubcatId);
        Category category = categoryRepository.findById(subcategoryDto.getCategoryId()).orElseThrow(RuntimeException::new);
        category.setSubcategories(Collections.singletonList(parentSubcategory));
        CatSubId catSubId = new CatSubId(category, childSubcategory);
        category.addCatSubId(catSubId);
        return saveAndCreateResponse(category);
    }

    @PostMapping("/createProductMapping")
    public ResponseEntity<String> createProduct(@RequestBody ProductDto productDto) {
        ObjectMapper mapper = new ObjectMapper();
        Product product = mapper.convertValue(productDto, Product.class);
        Subcategory subcategory = subCategoryRepository.findById(productDto.getSubCategoryId()).orElseThrow(RuntimeException::new);
        subcategory.setProducts(Collections.singletonList(product));
        Category category = categoryRepository.findById(productDto.getCategoryId()).orElseThrow(RuntimeException::new);
        category.setSubcategories(Collections.singletonList(subcategory));
        return saveAndCreateResponse(category);
    }

    private ResponseEntity<String> saveAndCreateResponse(Category category) {
        Optional.of(categoryRepository.save(category)).orElseThrow(RuntimeException::new);
        return ResponseEntity.ok("Success");
    }

}
