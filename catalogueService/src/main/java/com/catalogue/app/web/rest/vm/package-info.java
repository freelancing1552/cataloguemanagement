/**
 * View Models used by Spring MVC REST controllers.
 */
package com.catalogue.app.web.rest.vm;
