package com.catalogue.app.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the subcat_prod_id database table.
 * 
 */
@Entity
@Table(name="subcat_prod_id")
@NamedQuery(name="SubcatProdId.findAll", query="SELECT s FROM SubcatProdId s")
public class SubcatProdId extends AbstractAuditingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	//bi-directional many-to-one association to Product
	@ManyToOne(fetch=FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name="prod_id")
	private Product product;

	//bi-directional many-to-one association to Subcategory
	@ManyToOne(fetch=FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name="subcat_id")
	private Subcategory subcategory;

	public SubcatProdId() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Subcategory getSubcategory() {
		return this.subcategory;
	}

	public void setSubcategory(Subcategory subcategory) {
		this.subcategory = subcategory;
	}

}
